<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tindakanvaksin', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('peserta_nik');
            $table->foreign('peserta_nik')->references('nik')->on('peserta');

            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');

            $table->unsignedBigInteger('jenis_vaksin');
            $table->foreign('jenis_vaksin')->references('id')->on('jenisvaksin');

            $table->date('tgl_vaksin');

            $table->enum('dosis',['1','2','3']);

            $table->unsignedBigInteger('lokasi_vaksin');
            $table->foreign('lokasi_vaksin')->references('id')->on('lokasi');

            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tindakanvaksin');
    }
};
