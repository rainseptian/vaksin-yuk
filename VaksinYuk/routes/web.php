<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\jenisvaksin;
use App\Http\Controllers\peserta;
use App\Http\Controllers\users;
use App\Http\Controllers\tindakanvaksin;
use App\Http\Controllers\lokasi;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.peserta.list_sudah_vaksin');
});

Route::get('/peserta/sudah-vaksin', function () {
    return view('pages.peserta.list_sudah_vaksin');
});


//CRUD JENIS VAKSIN
Route::resource('jenisvaksin',jenisvaksin::class);
//CRUD PESERTA
Route::resource('peserta',peserta::class);
//CRUD USERS
Route::resource('users',users::class);
//CRUD TINDAKANVAKSIN
Route::resource('tindakanvaksin',tindakanvaksin::class);

//CRUD LOKASI
Route::resource('lokasi',lokasi::class);


//CRUD Profile
Route::resource('profile', ProfileController::class);


Auth::routes();
