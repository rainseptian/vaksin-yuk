@extends('pages.master')

@section('title')
    List Peserta Sudah Vaksin
@endsection

@section('content')
<a href="/peserta/create" class="btn btn-primary my-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Status Vaksin</th>
        <th scope="col">Jenis Vaksin</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <th>1</th>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>
                <form action="/cast/" method="POST">
                <a href="/cast/" class="btn btn-info btn-sm">Cetak Kartu</a>
                <a href="/cast/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
        </tr>
    </tbody>
  </table>
@endsection