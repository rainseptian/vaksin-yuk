@extends('pages.master')

@section('title')
    List Peserta Vaksin
@endsection

@section('searchBar')
<li class="nav-item w-100">
    <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
    <input type="text" class="form-control text-white" name="search" placeholder="Cari Peserta Vaksin">
    <div class="col-auto">
        <button type="submit" class="btn btn-primary mb-3">Search</button>
        <button type="submit" class="btn btn-secondary mb-3"> <a style="text-decoration: none; color: black;" href="/peserta"> All </a> </button>
    </div>
    </form>
</li>
@endsection

@section('content')
<a href="/peserta/create" class="btn btn-primary my-3">Add</a>

<table class="table text-white">
    <thead>
      <tr>
        <th scope="col" class="text-whites">NIK</th>
        <th scope="col" class="text-whites" >Nama</th>
        <th scope="col" class="text-whites">Tanggal Lahir</th>
        <th scope="col" class="text-whites">Jenis Kelamin</th>
        <th scope="col" class="text-whites">alamat</th>
        <th scope="col" class="text-whites">No. HP</th>
        <th scope="col" class="text-whites">Tempat Lahir</th>

      </tr>
    </thead>
    <tbody>
        @forelse ($peserta as $key=>$val)
        <tr>
            <th>{{$val->nik}}</th>
            <td>{{$val->nama}}</td>
            <td>{{$val->tgl_lahir}}</td>
            <td>{{$val->jk}}</td>
            <td>{{$val->alamat}}</td>
            <td>{{$val->no_hp}}</td>
            <td>{{$val->tempat_lahir}}</td>
            <td>
                <form action="/tindakanvaksin/create" method="POST">
                <a href="/tindakanvaksin/create" class="btn btn-info btn-sm">Tindakan vaksin</a>
                <a href="/peserta/edit/" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td> Tidak ada data</td>
            </tr>
        @endforelse

    </tbody>
  </table>
@endsection
