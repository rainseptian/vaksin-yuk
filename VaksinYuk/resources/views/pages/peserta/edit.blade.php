@extends('pages.master')

@section('title')
    Form Pendaftaran Peserta Vaksin
@endsection

@section('content')
<form action="/peserta/edit/{{$id}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>NIK</label>
        <input type="number" class="form-control @error('title') is-invalid @enderror text-white" name="nik">
    </div>

    @error('nik')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror text-white" name="nama">
    </div>

    @error('nama_lengkap')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Jenis Kelamin</label>
        <select name="jk" class="form-control col-3 text-white">
            <option value="">--Pilih Jenis Kelamin--</option>
            <option value="Laki-Laki">Laki-Laki</option>
            <option value="Perempuan">Perempuan</option>
        </select>
    </div>

    @error('jk')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Alamat Lengkap</label>
        <textarea name="alamat" class="form-control @error('title') is-invalid @enderror text-white" id=""></textarea>
    </div>


    @error('alamat')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Nomor Telepon</label>
        <input type="number" class="form-control @error('title') is-invalid @enderror text-white" name="no_hp">
    </div>

    @error('no_hp')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror
    <div class="form-group">
        <label>Tempat Lahir</label>
        <input type="text" class="form-control col-2 @error('title') is-invalid @enderror text-white" name="tempat_lahir">
    </div>

    @error('tempat_lahir')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control col-2 @error('title') is-invalid @enderror text-white" name="tgl_lahir">
    </div>

    @error('tgl_lahir')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror



    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
