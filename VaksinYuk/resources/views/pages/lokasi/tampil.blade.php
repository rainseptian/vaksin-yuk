@extends('pages.master')

@section('title')
    Tambah Lokasi Vaksin
@endsection

@section('content')
<a href="/lokasi/create" class="btn btn-primary my-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($lokasi as $key=>$val)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$val->lokasi}}</td>
            <td>
            <form action="/lokasi/{{$val->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/lokasi/{{$val->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
</form>
            </td>
        </tr>
        @empty
        <tr>
            <td>tidak ada data</td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection
