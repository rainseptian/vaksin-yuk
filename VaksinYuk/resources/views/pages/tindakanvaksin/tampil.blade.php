@extends('pages.master')

@section('title')
    List Tindakan Vaksin
@endsection

@section('content')
<a href="/tindakanvaksin/create" class="btn btn-primary my-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">NIK</th>
        <th scope="col">Petugas Input</th>
        <th scope="col">Jenis Vaksin</th>
        <th scope="col">Tanggal Vaksin</th>
        <th scope="col">Dosis</th>
        <th scope="col">Lokasi Vaksin</th>
        <th scope="col">Keterangan</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($tindakanvaksin as $key=>$val)
        <tr>
            <th>{{$key +1}}</th>
            <td>{{$val->peserta_nik}}</td>
            <td>{{$val->users_id}}</td>
            <td>{{$val->jenis_vaksin}}</td>
            <td>{{$val->dosis}}</td>
            <td>{{$val->lokasi_vaksin}}</td>
            <td>{{$val->keterangan}}</td>
            <td>
                <form action="/tindakanvaksin/" method="POST">
                <a href="/tindakanvaksin/" class="btn btn-info btn-sm">Detail</a>
                <a href="/tindakanvaksin/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td> Tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection