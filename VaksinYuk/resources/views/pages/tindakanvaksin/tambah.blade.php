@extends('pages.master')

@section('title')
    Form Tindakan Vaksin
@endsection

@section('content')
<form action="/tindakanvaksin/create" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label>NIK Peserta</label>
        <input type="number" class="form-control @error('title') is-invalid @enderror text-white" name="peserta_nik">
    </div>

    @error('peserta_nik')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Users ID</label>
        <input type="number" class="form-control @error('title') is-invalid @enderror text-white" name="users_id">
    </div>

    @error('users_id')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Jenis Vaksin</label>
        <select  class="form-control col-3 text-white" name="jenis_vaksin">
            <option value="">--Pilih jenis vaksin--</option>
            @forelse ($jenisvaksin as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Tidak ada data</option>
            @endforelse
        </select>
    </div>
    @error('jenis_vaksin')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror


    <div class="form-group">
        <label>Tanggal Vaksin</label>
        <input type="date" class="form-control col-2 @error('title') is-invalid @enderror text-white" name="tgl_vaksin">
    </div>

    @error('tgl_vaksn')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror


<div class="form-group">
        <label>Dosis</label>
        <select name="dosis" class="form-control col-3 text-white">
            <option value="">--Dosis--</option>
            <option value="1">Dosis Pertama</option>
            <option value="2">Dosis Kedua</option>
            <option value="3">Dosis Ketiga</option>
        </select>
    </div>

    @error('dosis')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror



    <div class="form-group">
        <label>Lokasi</label>
        <select  class="form-control col-3 text-white" name="lokasi">
            <option value="">--Pilih Lokasi Vaksin--</option>
            @forelse ($lokasi as $item)
                <option class="text-white" value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Tidak ada data</option>
            @endforelse
        </select>
    </div>

    @error('lokasi')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Catatan</label>
        <textarea name="keterangan" class="form-control @error('title') is-invalid @enderror text-white" id=""></textarea>
    </div>


    @error('keterangan')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
