@extends('pages.master')

@section('title')
    My Profile
@endsection

@section('searchBar')
<li class="nav-item w-100">
    <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
    <input type="text" class="form-control text-white" name="search" placeholder="Cari Peserta Vaksin">
    <div class="col-auto">
        <button type="submit" class="btn btn-primary mb-3">Search</button>
        <button type="submit" class="btn btn-secondary mb-3"> <a style="text-decoration: none; color: black;" href="/peserta"> All </a> </button>
    </div>
    </form>
</li>
@endsection

@section('content')  
      <div class="row">
        <div class="col-lg-4">
          <div class="card mb-4">
            <div class="card-body text-center">
              <img src="/images/{{ $profile[Auth::user()->id - 1]->foto_profil }}" alt="avatar"
                class="rounded-circle img-fluid" style="width: 150px;">
              <h5 class="my-3">{{ Auth::user()->name }}</h5>
              <p class="text-muted mb-1">{{ $profile[Auth::user()->id - 1]->jk }}</p>
            </div>
          </div>
          
        </div>
        <div class="col-lg-8">
          <div class="card mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Full Name</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0">{{ Auth::user()->name }}</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Email</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0">{{ Auth::user()->email }}</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Phone</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0">{{ $profile[Auth::user()->id - 1]->no_hp }}</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <p class="mb-0">Address</p>
                </div>
                <div class="col-sm-9">
                  <p class="text-muted mb-0">{{ $profile[Auth::user()->id - 1]->alamat }}</p>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
@endsection