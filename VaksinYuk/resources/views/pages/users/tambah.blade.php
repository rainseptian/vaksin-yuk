@extends('pages.master')

@section('title')
    Form Tambah Data Users
@endsection

@section('content')
<form action="/users" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror text-white" name="name">
    </div>

    @error('name')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror text-white" name="email">
    </div>

    @error('email')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Password</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror text-white" name="password">
    </div>

    @error('password')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Level Users</label>
        <select name="level" class="form-control col-3 text-white">
            <option value="">--Pilih Level User--</option>
            <option value="admin">Admin</option>
            <option value="pegawai">Pegawai</option>
        </select>
    </div>

    @error('level')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    
    



    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
