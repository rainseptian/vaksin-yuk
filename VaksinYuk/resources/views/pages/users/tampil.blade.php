@extends('pages.master')

@section('title')
    Data User
@endsection

@section('content')
<a href="/users/create" class="btn btn-primary my-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">ID Users</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Password</th>
        <th scope="col">Level Users</th>
        

      </tr>
    </thead>
    <tbody>
        @forelse ($users as $key=>$val)
        <tr>
            <th>{{$key+1}}</th>
            <td>{{$val->name}}</td>
            <td>{{$val->email}}</td>
            <td>{{$val->password}}</td>
            <td>{{$val->level}}</td>
            
            <td>
                <form action="/users/{{$val->id}}" method="POST">
                <a href="/users/{{$val->id}}/" class="btn btn-info btn-sm">Detail</a>
                <a href="/users/{{$val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td> Tidak ada data</td>
            </tr>
        @endforelse

    </tbody>
  </table>
@endsection
