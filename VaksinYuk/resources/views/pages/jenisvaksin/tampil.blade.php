@extends('pages.master')

@section('title')
    Data Jenis Vaksin
@endsection

@section('content')
<a href="/jenisvaksin/create" class="btn btn-primary my-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($jenisvaksin as $key=>$val)
        <tr>
            <td>{{$key + 1}}</td>
            <td class="text-white">{{$val->nama_vaksin}}</td>
            <td>

            <form action="/jenisvaksin/{{$val->id}}" method ="POST">
                @csrf
                @method('delete')
                <a href="/jenisvaksin/{{$val->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
</form>
            </td>
        </tr>
        @empty
        <tr>
            <td>tidak ada data</td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection
