<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Vaksin Yuk</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{asset('template/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/assets/vendors/css/vendor.bundle.base.css')}}">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="{{asset('template/assets/vendors/jvectormap/jquery-jvectormap.css')}}">
        <link rel="stylesheet" href="{{asset('template/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/assets/vendors/owl-carousel-2/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/assets/vendors/owl-carousel-2/owl.theme.default.min.css')}}">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="{{asset('template/assets/css/style.css')}}">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="{{asset('template/assets/images/favicon.png')}}" />
    </head>
    <body>
        <div class="container-scroller">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">

            <h4> <a href="/peserta" style="text-decoration: none; color: white;">Vaksin Yuk</a></h4>
            </div>
            <ul class="nav">
            <!-- Divider -->
            <div class="dropdown-divider"></div>
            <li class="nav-item profile">
                <div class="profile-desc">
                <div class="profile-pic">
                    <div class="count-indicator">
                        <a href="/profile">
                            <img class="img-xs rounded-circle " src="{{asset('/template/assets/images/faces/face15.jpg')}}" alt="">
                        </a>
                    <span class="count bg-success"></span>
                    </div>
                    <div class="profile-name">
                    <h5 class="mb-0 font-weight-normal"><a style="text-decoration: none; color: white;" href="/profile">Henry Klein</a></h5>
                    <span>Gold Member</span>
                    </div>
                </div>
                </div>
            </li>
            <!-- Divider -->
            <div class="dropdown-divider"></div>
            <li class="nav-item nav-category">
                <span class="nav-link">Navigation</span>
            </li>
            <li class="nav-item menu-items">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-icon">
                    <i class="mdi mdi-human"></i>
                </span>
                <span class="menu-title">Peserta</span>
                <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="/peserta">Peserta Vaksin</a></li>
                    <li class="nav-item"> <a class="nav-link" href="/peserta/sudah-vaksin">List Peserta Sudah Vaksin</a></li>
                </ul>
                </div>
            </li>
            <li class="nav-item menu-items">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic2" aria-expanded="false" aria-controls="ui-basic2">
                <span class="menu-icon">
                    <i class="mdi mdi-laptop"></i>
                </span>
                <span class="menu-title">Master Data</span>
                <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic2">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="/jenisvaksin">Jenis Vaksin</a></li>
                    <li class="nav-item"> <a class="nav-link" href="/lokasi">Lokasi</a></li>
                </ul>
                </div>
            </li>

            <li class="nav-item menu-items">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <span class="menu-icon ">
                        <i class="mdi mdi-logout"></i>
                    </span>
                    <span class="menu-title">Logout</span>
                </a>
        
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar p-0 fixed-top d-flex flex-row">
            <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
                <a >Vaksin Yuk</a>
            </div>
            <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-menu"></span>
                </button>
                <ul class="navbar-nav w-100">
                    @yield('searchBar')
                </ul>
                <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item dropdown d-none d-lg-block">
                    <a class="btn btn-success" style="--bs-btn-font-size: .75rem;" href="/peserta/create">+ Daftar Vaksin</a>
                </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-format-line-spacing"></span>
                </button>
            </div>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div><h1>@yield('title')</h1></div>
                @yield('content')

                </div>

            </div>
                    </div>
                    </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin templates</a> from Bootstrapdash.com</span>
                </div>
            </footer>
            <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="{{asset('template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page -->
        <script src="{{asset('template/assets/vendors/chart.js/Chart.min.js')}}"></script>
        <script src="{{asset('template/assets/vendors/progressbar.js/progressbar.min.js')}}"></script>
        <script src="{{asset('template/assets/vendors/jvectormap/jquery-jvectormap.min.js')}}"></script>
        <script src="{{asset('template/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <script src="{{asset('assets/vendors/owl-carousel-2/owl.carousel.min.js')}}"></script>
        <!-- End plugin js for this page -->
        <!-- inject:js -->
        <script src="{{asset('template/assets/js/off-canvas.js')}}"></script>
        <script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
        <script src="{{asset('template/assets/js/misc.js')}}"></script>
        <script src="{{asset('template/assets/js/settings.js')}}"></script>
        <script src="{{asset('assets/js/todolist.js')}}"></script>
        <!-- endinject -->
        <!-- Custom js for this page -->
        <script src="{{asset('assets/js/dashboard.js')}}"></script>
        <!-- End custom js for this page -->
    </body>
</html>
