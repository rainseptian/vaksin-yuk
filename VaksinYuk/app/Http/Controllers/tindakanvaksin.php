<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Peserta;
use App\Models\jenisvaksin;
use App\Models\lokasiModel;



class tindakanvaksin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tindakanvaksin =DB::table('tindakanvaksin')->get();
        return view('pages.tindakanvaksin.tampil',['tindakanvaksin'=>$tindakanvaksin]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenisvaksin = jenisvaksin::all();
        $lokasi = lokasiModel::all();
        return view("pages.tindakanvaksin.tambah", ['jenisvaksin'=>$jenisvaksin,'lokasi'=>$lokasi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'peserta_nik'=>'required',
            'users_id'=>'required',
            'jenis_vaksin'=>'required',
            'tgl_vaksin'=>'required',
            'dosis'=>'required',
            'lokasi_vaksin'=>'required',
            'keterangan'=>'required',
        ]);

        DB::table('tindakanvaksin')->insert([
            'peserta_nik'=>$request['peserta_nik'],
            'users_id'=>$request['users_id'],
            'jenis_vaksin'=>$request['jenisvaksin'],
            'tgl_vaksin'=>$request['tgl_vaksin'],
            'dosis'=>$request['dosis'],
            'lokasi_vaksin'=>$request['lokasi_vaksin'],
            'keterangan'=>$request['keterangan'],
        ]);
        $jenisvaksin = new jenisvaksin;
        $jenisvaksin->name = $request->nama_vaksin;
        return redirect('/tindakanvaksin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
