<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class peserta extends Model
{
    protected $table = 'peserta';
    protected $fillable = ['nik','nama','jk','tempat_lahir','tgl_lahir','alamat','no_hp'];
}
