<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jenisvaksin extends Model
{
    protected $table='jenisvaksin';
    protected $fillable=['nama_vaksin'];
    use HasFactory;
}
